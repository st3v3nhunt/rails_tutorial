class Relationship < ActiveRecord::Base
  attr_accessible :followed_id
  
  belongs_to :follower, :class_name => "User"
  belongs_to :followed, :class_name => "User"

  validates :follower_id, :presence => true
  validates :followed_id, :presence => true
  
  def self.from_users_followed_by(user)
    followed_ids = user.following.map(&:id).join(",")
    where("user_id IN (#{followed_ids}) OR user_id = ?", user)
  end
end